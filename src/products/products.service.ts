import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) //เพิ่ม
    private ProductRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto): Promise<Product> {
    const product: Product = new Product();
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    return this.ProductRepository.save(product);
  }

  findAll(): Promise<Product[]> {
    return this.ProductRepository.find();
  }

  findOne(id: number): Promise<Product> {
    return this.ProductRepository.findOneBy({ id: id });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.ProductRepository.findOneBy({ id: id });
    const updatedProduct = { ...product, ...updateProductDto };
    return this.ProductRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.ProductRepository.findOneBy({ id: id });
    return this.ProductRepository.remove(product);
  }
}
